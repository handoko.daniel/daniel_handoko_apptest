import React, { } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Constant from '../../utilities/Constant';

export default function ContactCard(props) {
  function renderImage() {
    const { photo } = props.data;
    if (photo == 'N/A') {
      return (
        <Image
          source={{ uri: 'https://images.unsplash.com/photo-1549706844-23f9ee754ce2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTMyfHxwZXJzb258ZW58MHwyfDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60' }}
          style={localStyles.profilePicture}
        />
      )
    }
    else {
      return (
        <Image
          source={{ uri: photo }}
          style={localStyles.profilePicture}
        />
      )
    }
  }

  return (
    <View style={localStyles.container}>
      {renderImage()}
      <View style={localStyles.infoContainer}>
        <Text style={localStyles.boldLabel}>{props.data.firstName} {props.data.lastName}</Text>
        <Text>Age: {props.data.age}</Text>
        <TouchableOpacity
          style={localStyles.deleteButton}
          onPress={props.onPress}
        >
          <Text style={{ color: Constant.Color.ABSOLUTE_WHITE }}>Delete</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const localStyles = StyleSheet.create({
  container: {
    padding: 16,
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
    width: "100%",
    flexDirection: 'row',
    borderBottomWidth: 1
  },
  profilePicture: {
    width: 80,
    height: 80,
    borderRadius: 40,
    resizeMode: 'cover',
  },
  infoContainer: {
    flex: 1,
    paddingHorizontal: 16,
    justifyContent: 'center',
  },
  boldLabel: {
    fontWeight: 'bold',
    fontSize: 16,
    marginBottom: 8
  },
  deleteButton: {
    width: 80,
    marginVertical: 8,
    padding: 8,
    alignItems: 'center',
    backgroundColor: Constant.Color.SECONDARY_PRIMARY,
    justifyContent: 'center',
  }
})