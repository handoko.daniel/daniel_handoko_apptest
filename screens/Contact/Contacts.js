import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import ApiHelper from '../../services/ApiHelper';
import ContactCard from './ContactCard';

export default function Contacts(props) {

  async function handleDeleteContact(contact){
    // let deleteContactResponse = await ApiHelper.deleteApiResponse("contact/", contact.id);
    console.log(contact);
    props.onDeleteContact()
  }

  return (
    <FlatList
      contentContainerStyle={{ padding: 16 }}
      data={props.data}
      renderItem={({ item, index }) =>
        <ContactCard
          key={index}
          data={item}
          onPress={() => handleDeleteContact(item)}
        />
      }
    />
  );

}
