import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Ionicons';

// import custom component
import Constant from '../../utilities/Constant';
import LabeledTextInput from '../../components/forms/LabeledTextInput';
import PrimaryButton from '../../components/forms/PrimaryButton';
import ApiHelper from '../../services/ApiHelper';

export default function CreateNewContact(props) {
  // begin state declaration
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [age, setAge] = useState(0);
  const [photo, setPhoto] = useState('N/A');

  const navigation = useNavigation();
  // end of state declaration

  const handleChangeText = (id, value) => {
    if (id == "firstName") {
      setFirstName(value);
    }
    else if (id == "lastName") {
      setLastName(value);
    }
    else if (id == "age") {
      setAge(value);
    }
    else if (id == "photo") {
      setPhoto(value);
    }
  }

  const handleGetPictureUri = (value) => {
    if (value != null) {
      setPhoto(value);
    }
  }

  function handleRedirectUser(page) {
    if (page == "TakePicture") {
      navigation.navigate(page, { onPictureTaken: handleGetPictureUri })
    }
    else if (page == "Back") {
      navigation.goBack();
    }
  }

  async function handleSubmitData() {
    let contact = {
      "firstName": firstName,
      "lastName": lastName,
      "age": age,
      "photo": photo == 'N/A' ? photo : photo.uri,
    }
    await ApiHelper.postApiResponse("contact", contact);
    props.route.params.onCreateNewContact();
    handleRedirectUser("Back")
  }

  function renderImage() {
    if (photo == 'N/A') {
      return (
        <TouchableOpacity
          onPress={() => handleRedirectUser('TakePicture')}
          style={localStyles.photo}
        >
          <Text>No Photo</Text>
        </TouchableOpacity>
      )
    }
    else {
      return (
        <Image
          style={localStyles.photo}
          source={{ uri: photo.uri }}
        />
      )
    }
  }

  return (
    <SafeAreaView style={localStyles.container}>
      <Icon
        name='ios-chevron-back'
        size={32}
        style={{ marginBottom: 8 }}
        onPress={() => handleRedirectUser('Back')}
      />
      {renderImage()}
      <LabeledTextInput
        id="firstName"
        label="First Name"
        placeholder="First Name"
        value={firstName}
        onChangeText={handleChangeText}
        type="active"
        autoCapitalize="none"
        isActive={true}
        required={true}
      />
      <LabeledTextInput
        id="lastName"
        label="Last Name"
        placeholder="Last Name"
        value={lastName}
        onChangeText={handleChangeText}
        type="active"
        autoCapitalize="none"
        isActive={true}
        required={true}
      />
      <LabeledTextInput
        id="age"
        label="Age"
        placeholder="Age"
        value={age.toString()}
        onChangeText={handleChangeText}
        type="active"
        autoCapitalize="none"
        isActive={true}
        required={true}
      />
      <PrimaryButton
        label="Save"
        isActive
        onPress={() => handleSubmitData()}
      />
    </SafeAreaView>
  )
}

const localStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
    borderRadius: 8,
    padding: 16,
    width: Constant.Layout.screenWidth
  },
  photo: {
    width: Constant.Layout.screenWidth * 0.3,
    height: Constant.Layout.screenWidth * 0.3,
    borderRadius: 8,
    marginBottom: 8,
    backgroundColor: Constant.Color.PRIMARY,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center'
  },
  label: {
    marginVertical: 16,
    textAlign: 'center',
    marginTop: 32
  }
})