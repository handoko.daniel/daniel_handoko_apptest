import { Dimensions } from "react-native"
export default {
  //Authentication
  USER_TOKEN: 'USER_TOKEN',

  Color: {
    PRIMARY: '#C0ECFF',
    DARK_PRIMARY: '#275DAD',
    SECONDARY_PRIMARY: '#F02D3A',
    PRIMARY_COMPLEMENTER: '#6153CC',
    ABSOLUTE_WHITE: '#FFFFFF',
    THIN_GRAY: 'rgba(245, 245, 245, 0.8)',
    DARK_GRAY: '#898A8D',
    ABSOLUTE_BLACK: '#111111',
    DARK_LABEL: '#52617D',
    THIN_LABEL: '#c4c4c4',
  },

  Layout: {
    screenWidth: Dimensions.get('window').width,
    screenHeight: Dimensions.get('window').height,
  }
}