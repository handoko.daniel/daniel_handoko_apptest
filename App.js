import { StatusBar } from 'expo-status-bar';
import AppNavigator from './navigations/AppNavigator'

export default function App() {
  return (
    <AppNavigator />
  );
}
