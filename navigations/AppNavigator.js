import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Dashboard from '../screens/Dashboard/Dashboard';
import CreateNewContact from '../screens/Contact/CreateNewContact';
import TakePicture from '../screens/Contact/TakePicture';

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Dashboard" component={Dashboard} options={{ headerShown: false }} />
        <Stack.Screen name="CreateNewContact" component={CreateNewContact} options={{ headerShown: false }} />
        <Stack.Screen name="TakePicture" component={TakePicture} options={{ headerShown: false }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;