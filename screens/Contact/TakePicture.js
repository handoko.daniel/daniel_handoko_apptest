import { useNavigation } from '@react-navigation/native';
import { Camera, CameraType } from 'expo-camera';
import { useState, useRef } from 'react';
import { Button, StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Ionicons';
import * as ImagePicker from 'expo-image-picker';

// import custom component
import Constant from '../../utilities/Constant';

export default function TakePicture(props) {
  const [type, setType] = useState(CameraType.back);
  const [permission, requestPermission] = Camera.useCameraPermissions();
  const [photo, setPhoto] = useState(null)

  const navigation = useNavigation();
  const camera = useRef();

  if (!permission) {
    // Camera permissions are still loading
    return <View />;
  }

  if (!permission.granted) {
    // Camera permissions are not granted yet
    return (
      <View style={[localStyles.container, { justifyContent: 'center', }]}>
        <Text style={{ textAlign: 'center' }}>
          We need your permission to show the camera
        </Text>
        <Button onPress={requestPermission} title="grant permission" />
      </View>
    );
  }

  function toggleCameraType() {
    setType((current) => (
      current === CameraType.back ? CameraType.front : CameraType.back
    ));
  }

  async function handleTakePicture() {
    let photoTaken = await camera.current.takePictureAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      quality: 0.6,
      allowsEditing: true,
      aspect: [1, 1]
    });
    setPhoto(photoTaken)
  }
  function handleRedirectUser(page) {
    if (page == "TakePicture") {
      navigation.navigate(page, { onPictureTaken: () => handleGetPictureUri })
    }
    else if (page == "Back") {
      navigation.goBack();
    }
  }

  function handleSubmitPhoto() {
    props.route.params.onPictureTaken(photo);
    handleRedirectUser("Back");
  }

  function renderPhotoTaken() {
    if (photo == null) {
      return (
        <Camera
          style={localStyles.camera}
          type={type}
          ratio="1:1"
          ref={camera}
        />
      )
    }
    else {
      return (
        <Image
          source={{ uri: photo.uri }}
          style={localStyles.camera}
        />
      )
    }
  }

  function renderCaptureButton() {
    if (photo == null) {
      return (
        <TouchableOpacity
          style={localStyles.button}
          onPress={() => handleTakePicture()}>
        </TouchableOpacity>
      )
    }
    else {
      return (
        <TouchableOpacity
          style={localStyles.button}
          onPress={() => handleSubmitPhoto()}
        >
          <Text style={localStyles.text}>Submit</Text>
        </TouchableOpacity>
      )
    }
  }

  return (
    <SafeAreaView style={localStyles.container}>
      <Icon
        name='ios-chevron-back'
        size={32}
        style={{ marginBottom: 8 }}
        onPress={() => handleRedirectUser('Back')}
      />
      {renderPhotoTaken()}
      <View style={localStyles.buttonContainer}>
        <Icon
          name="ios-refresh"
          size={24}
          onPress={() => setPhoto(null)}
          style={{ marginRight: 16 }}
        />
        {renderCaptureButton()}
        <Icon
          name="ios-repeat"
          size={24}
          onPress={toggleCameraType}
          style={{ marginLeft: 16 }}
        />
      </View>
    </SafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  container: {
    flex: 1,
    width: Constant.Layout.screenWidth,
    padding: 16,
  },
  camera: {
    width: "100%",
    height: Constant.Layout.screenWidth
  },
  buttonContainer: {
    width: Constant.Layout.screenWidth,
    padding: 16,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  button: {
    width: 80,
    height: 80,
    alignItems: 'center',
    backgroundColor: Constant.Color.DARK_PRIMARY,
    borderRadius: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 16,
    color: 'white',
  },
})