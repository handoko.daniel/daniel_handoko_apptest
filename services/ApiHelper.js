// import custom component
import axios from 'axios';

const BASE_URL = 'https://simple-contact-crud.herokuapp.com/'

class ApiHelper {
  static async getApiResponse(route) {
    var data = null;
    await axios.get(BASE_URL + route)
      .then(function (response) {
        data = response.data;
      })
      .catch(function () {
        data = null;
      })
      return data;
  }
  static async postApiResponse(route, body) {
    var data = null;
    console.log(BASE_URL + route);
    await axios.post(BASE_URL + route, body)
      .then(function (response) {
        data = response.data;
      })
      .catch(function () {
        data = null;
      })
      return data;
  }
  static async updateApiResponse(route, id) {
    var data = null;
    await axios.put(BASE_URL + route + id)
      .then(function (response) {
        data = response.data;
      })
      .catch(function () {
        data = null;
      })
      return data;
  }
  static async deleteApiResponse(route, body) {
    var data = null;
    console.log(BASE_URL + route + body);
    await axios.delete(BASE_URL + route + body)
      .then(function (response) {
        data = response.data;
      })
      .catch(function (error) {
        console.log("error: ", error);
        data = null;
      })
      return data;
  }
}

export default ApiHelper;