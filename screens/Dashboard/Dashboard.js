import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { View, FlatList, RefreshControl, StyleSheet } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Modal from 'react-native-modal';
import { useNavigation } from '@react-navigation/native';

// import custom component
import PrimaryButton from '../../components/forms/PrimaryButton';
import ApiHelper from '../../services/ApiHelper';
import Constant from '../../utilities/Constant';
import Contacts from '../Contact/Contacts';
import LoaderModal from '../../components/modal/LoaderModal';

export default function Dashboard() {
  // begin state declaration
  const [data, setData] = useState([]);
  const [refreshState, setRefreshState] = useState(false);
  const [modalStatus, setModalStatus] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const navigation = useNavigation();
  // end of state declaration

  useEffect(() => {
    handleLoadData();
  }, [])

  async function handleLoadData() {
    setIsLoading(true)
    setModalStatus(true)
    let contactResponse = await ApiHelper.getApiResponse("contact")
    setData(contactResponse.data);
    setModalStatus(false)
    setRefreshState(false)
  }

  function handleRedirectUser(page) {
    if (page == "CreateNewContact") {
      navigation.navigate(page, { onCreateNewContact: handleSubmitNewContact })
    }
  }

  const handleSubmitNewContact = async () => {
    handleLoadData();
  }

  function renderModal() {
    return (
      <Modal
        isVisible={modalStatus}
        animationIn="bounceIn"
        animationOut="bounceOut"
        animationInTiming={1000}
        animationOutTiming={500}
        onBackdropPress={() => setModalStatus(false)}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          padding: 10
        }}
      >
        <LoaderModal />
      </Modal>
    )
  }

  return (
    <SafeAreaView style={localStyles.container}>
      <StatusBar
        style='dark'
        backgroundColor={Constant.Color.ABSOLUTE_WHITE}
      />

      <FlatList
        refreshControl={
          <RefreshControl
            refreshing={refreshState}
            onRefresh={() => handleLoadData()}
          />
        }
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={
          <>
            <Contacts
              data={data}
              onDeleteContact={() => handleLoadData()}
            />
          </>
        }
      />
      <View style={{ padding: 16 }}>
        <PrimaryButton
          isActive
          label="Create New Contact"
          onPress={() => handleRedirectUser('CreateNewContact')}
        />
      </View>
      {renderModal()}
    </SafeAreaView>
  );
}

const localStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constant.Color.ABSOLUTE_WHITE
  },
})
