import React from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';

// import custom component
import Constant from '../../utilities/Constant';

export default function LoaderModal() {
  return (
    <View style={localStyles.container}>
      <ActivityIndicator
        size="large"
        color={Constant.Color.PRIMARY}
      />
      <Text style={localStyles.label}>Memuat Data</Text>
    </View>
  )
}

const localStyles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: Constant.Color.ABSOLUTE_WHITE,
    borderRadius: 8,
    height: Constant.Layout.screenWidth * 0.6,
    justifyContent: 'center',
    padding: 16,
    width: Constant.Layout.screenWidth * 0.6,
  },
  label: {
    marginVertical: 16,
    textAlign: 'center',
    marginTop: 32
  }
})